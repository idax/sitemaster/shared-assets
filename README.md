# Shared Assets

These are third-party libraries, multimedia resources such as fonts and graphics, that are shared
across multiple sites to perform common functionalities and create a uniform branding.

# Assets and Versions

|Asset Name | Type             | Version |
| --------- | ---------------- | ------- |
| jQuery    | javascript       | 3.5.0   |
| Bootstrap | CSS + javascript | 4.5.2   |
