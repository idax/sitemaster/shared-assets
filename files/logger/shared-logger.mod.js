import Logger from "./js-logger.mod.js";

Logger.useDefaults();
Logger.setLevel(Logger.DEBUG);

export default Logger;